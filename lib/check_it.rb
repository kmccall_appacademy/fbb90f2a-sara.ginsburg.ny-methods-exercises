def dec_remainder_of_two_floats(f_dividend, f_divisor)
  parts = (f_dividend / f_divisor).to_s.split(".")

  return parts.length == 2 ? parts[1].to_i : 0
end

puts dec_remainder_of_two_floats(50.0, 8.0)
